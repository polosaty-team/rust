extern crate hyper;

#[macro_use]
extern crate log;

use log::{info, trace, warn};

use std::io::Write;
use hyper::header::ContentLength;
use hyper::server::{Server, Request, Response};

use std::{thread, time};
use std::io::Read;


fn hello(mut req: Request, mut res: Response) {
    // handle things here
    let sleep_time = time::Duration::from_secs(3);
    // let now = time::Instant::now();
    // let msg = format!("{}", req);

    let mut body = String::new();
    req.read_to_string(&mut body).unwrap();
    println!("before sleep {} {}", body, req.uri);
    trace!("before sleep {} {}", body, req.uri);
    // println!("before sleep{}", body);
    thread::sleep(sleep_time);
    println!("after sleep {} {}", body, req.uri);

    let body = b"Hello World!";
    res.headers_mut().set(ContentLength(body.len() as u64));
    let mut res = res.start().unwrap();
    res.write_all(body).unwrap();
}

fn main() {
    Server::http("0.0.0.0:8081").unwrap().handle(hello).unwrap();
}

